package com.stone.crm.domain;

import java.math.BigDecimal;
import com.stone.common.annotation.Excel;
import com.stone.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 回款对象 st01_crm_receivables
 * 
 * @author stone
 * @date 2024-04-25
 */
public class St01_crm_receivables extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 回款ID */
    private String receivablesId;

    /** 回款编号 */
    @Excel(name = "回款编号")
    private String number;

    /** 回款计划ID */
    @Excel(name = "回款计划ID")
    private String planId;

    /** 客户ID */
    @Excel(name = "客户ID")
    private String customerId;

    /** 合同ID */
    @Excel(name = "合同ID")
    private String contractId;

    /** 回款日期 */
    @Excel(name = "回款日期")
    private String returnTime;

    /** 回款方式 */
    @Excel(name = "回款方式")
    private String returnType;

    /** 回款金额 */
    @Excel(name = "回款金额")
    private BigDecimal money;

    /** 状态;0 未审核 1 审核通过 2 审核拒绝 3 审核中 4 已撤回 */
    @Excel(name = "状态;0 未审核 1 审核通过 2 审核拒绝 3 审核中 4 已撤回")
    private String checkStatus;

    /** 审核记录ID */
    @Excel(name = "审核记录ID")
    private String examineRecordId;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBy;

    /** 负责人 */
    @Excel(name = "负责人")
    private String ownerUserId;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updatedBy;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updatedTime;

    public void setReceivablesId(String receivablesId) 
    {
        this.receivablesId = receivablesId;
    }

    public String getReceivablesId() 
    {
        return receivablesId;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setPlanId(String planId) 
    {
        this.planId = planId;
    }

    public String getPlanId() 
    {
        return planId;
    }
    public void setCustomerId(String customerId) 
    {
        this.customerId = customerId;
    }

    public String getCustomerId() 
    {
        return customerId;
    }
    public void setContractId(String contractId) 
    {
        this.contractId = contractId;
    }

    public String getContractId() 
    {
        return contractId;
    }
    public void setReturnTime(String returnTime) 
    {
        this.returnTime = returnTime;
    }

    public String getReturnTime() 
    {
        return returnTime;
    }
    public void setReturnType(String returnType) 
    {
        this.returnType = returnType;
    }

    public String getReturnType() 
    {
        return returnType;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setCheckStatus(String checkStatus) 
    {
        this.checkStatus = checkStatus;
    }

    public String getCheckStatus() 
    {
        return checkStatus;
    }
    public void setExamineRecordId(String examineRecordId) 
    {
        this.examineRecordId = examineRecordId;
    }

    public String getExamineRecordId() 
    {
        return examineRecordId;
    }
    public void setCreatedBy(String createdBy) 
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() 
    {
        return createdBy;
    }
    public void setOwnerUserId(String ownerUserId) 
    {
        this.ownerUserId = ownerUserId;
    }

    public String getOwnerUserId() 
    {
        return ownerUserId;
    }
    public void setCreatedTime(String createdTime) 
    {
        this.createdTime = createdTime;
    }

    public String getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(String updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(String updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedTime() 
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("receivablesId", getReceivablesId())
            .append("number", getNumber())
            .append("planId", getPlanId())
            .append("customerId", getCustomerId())
            .append("contractId", getContractId())
            .append("returnTime", getReturnTime())
            .append("returnType", getReturnType())
            .append("money", getMoney())
            .append("remark", getRemark())
            .append("checkStatus", getCheckStatus())
            .append("examineRecordId", getExamineRecordId())
            .append("createdBy", getCreatedBy())
            .append("ownerUserId", getOwnerUserId())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
